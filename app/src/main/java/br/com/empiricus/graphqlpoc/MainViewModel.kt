package br.com.empiricus.graphqlpoc

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.rx3.rxQuery
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor


class MainViewModel : ViewModel() {

    val listOfCountries = MutableLiveData<List<GetCountriesQuery.Country>>()

    val countryData = MutableLiveData<GetCountryDataQuery.Country>()

    val apolloServer = ApolloClient.builder()
        .okHttpClient(
            OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
                .build()
        )
        .serverUrl("https://countries.trevorblades.com/")
        .build()

    fun getAllContries() {
        apolloServer.rxQuery(GetCountriesQuery())
            .subscribeOn(Schedulers.io())
            .subscribe({
                listOfCountries.postValue(it.data?.countries())
            }, {
                it.printStackTrace()
            })
    }

    fun getRandomCountryData() {
        listOfCountries.value?.random()?.let {
            apolloServer.rxQuery(GetCountryDataQuery(it.code))
                .subscribe({
                    countryData.postValue(it.data?.country)
                }, {
                    it.printStackTrace()
                })
        }
    }
}