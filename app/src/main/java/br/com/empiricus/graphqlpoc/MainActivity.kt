package br.com.empiricus.graphqlpoc

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.button
import kotlinx.android.synthetic.main.activity_main.capital
import kotlinx.android.synthetic.main.activity_main.lingua
import kotlinx.android.synthetic.main.activity_main.moeda
import kotlinx.android.synthetic.main.activity_main.nome

class MainActivity : AppCompatActivity() {

    private val viewModel = MainViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupObserver()
        viewModel.getAllContries()
    }

    private fun setupObserver() {
        viewModel.listOfCountries.observe(this) {
            if (it.isNotEmpty()) {
                button.run {
                    isEnabled = true
                    setOnClickListener { viewModel.getRandomCountryData() }
                }
            }
        }

        viewModel.countryData.observe(this) {
            lingua.text = it.languages.first().name
            nome.text = it.name
            moeda.text = it.currency
            capital.text = it.capital
        }
    }
}